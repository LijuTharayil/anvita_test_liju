<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    //
    protected $fillable = ['name', 'image'];
    protected $appends = ['image_path'];
    protected $visible = ['name', 'image', 'id', 'image_path'];


    public function getImagePathAttribute()
    {
        return url('storage/uploads/'.$this->image);
    }
}
