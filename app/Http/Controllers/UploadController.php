<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;
use \Softon\LaravelFaceDetect\Facades\FaceDetect;
use Validator;

class UploadController extends Controller
{
    //

    public function getImages()
    {
        $user_images = Upload::all();
        return view('upload', compact('user_images'));
    }




    public function uploadImage(Request $request)
    {


        Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png',
        ])->validate();



        $path = $request->file('image')->store('public/uploads');
        $upload = new Upload();
        $upload->name = $request->name;
        $upload->image = str_replace("public/uploads/","",$path);
        $upload->save();

        if($upload)
        {
            return back()->with('status', 'User Image Added!');
        }
        else
        {
            return back()->with('status', 'Try Again Later!');
        }

    }

    public function edit($id)
    {
        $upload = Upload::find($id);
        $user_images = Upload::all();
        return view('upload', compact('upload', 'user_images'));
    }

    public function updateImage(Request $request, $id)
    {


        Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
        ])->validate();




        $upload = Upload::findOrFail($id);

        $old_image = $upload->image;

        $upload->name = $request->name;
        if($request->file('image')) {
            $path = $request->file('image')->store('public/uploads');
            $upload->image = str_replace("public/uploads/", "", $path);
            $image_path = public_path().'/storage/uploads/'.$old_image;
            unlink($image_path);
        }
        $upload->save();

        if($upload)
        {
            return back()->with('status', 'User Image Updated!');
        }
        else
        {
            return back()->with('status', 'Try Again Later!');
        }

    }




    public function deleteimage($id)
    {

        $upload = Upload::findOrFail($id);
        $image_path = public_path().'/storage/uploads/'.$upload->image;
        unlink($image_path);
        $upload->delete();


        // redirect
        return back()->with('status', 'Image deleted!');
    }
}
