<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/upload_image', 'UploadController@getImages');

Route::get('/edit/{id}', 'UploadController@edit');

Route::post('/upload_image', 'UploadController@uploadImage');

Route::post('/update/{id}', 'UploadController@updateImage');

Route::delete('/delete_image/{id}/', 'UploadController@deleteimage');

