<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }} || Upload Form</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Upload Form
                </div>

                <div class="links">
                    <a href="{{ url('/') }}">Back To Home</a>   @if(isset($upload)) ||
                    <a href="{{ url('/upload_image') }}">Add Images</a>
                                                                    @endif
                </div><br> <br><br> <br>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(isset($upload))
                    <form method="POST" action="/update/{{ $upload->id }}" enctype="multipart/form-data">
                    @else

                            <form method="POST" action="/upload_image" enctype="multipart/form-data">
               @endif
                    @csrf

                    <input type="tex" placeholder="Name" name="name" @if(isset($upload)) value="{{ $upload->name }}" @endif>  <br> <br>
                    <input type="file" name="image" accept="image/*"> <br> <br>
                            @if(isset($upload)) <img src="{{ $upload->image_path }}" style="width: 200px;"> @endif
                            <input type="submit" name="submit" >
                </form>
<hr><hr>
<br> <br>
                @foreach($user_images as $user_image)

                    <img src="{{ $user_image->image_path }}" alt="{{ $user_image->name }}" style="width: 200px;"><br>
                    {{ $user_image->name }}  || <a href="{{ url('edit/'.$user_image->id) }}"> Edit</a> ||
                    {{ Form::open(array('style' => 'display:inline-block;', 'url' => 'delete_image/'.$user_image->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger','onclick'=>"return confirm('Are you sure ?');")) }}
                    {{ Form::close() }}
                    <br> <br> <hr> <br>



              @endforeach



            </div>
        </div>
    </body>
</html>
